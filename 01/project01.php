<?php

// application/libraries/App/User/Service/User.php:1427
class User_Service_User
{
    public function userBan() {
// ......
        if ($typeBan == BAN) {
            $adminAction = Admin_Service_Log::EVENT_ADMIN_BAN_USER;
        } elseif (self::isLimitingBan($userData['ban'])) {
            $adminAction = Admin_Service_Log::EVENT_ADMIN_SET_ACCOUNT_LIMIT;
        } else {
            $adminAction = Admin_Service_Log::EVENT_ADMIN_BAN_USER;
        }
       
// ......
        $this->changeUserStatus();
        
        //Перетаскиваем полуактивные заказы в черновики
        if ($typeBan == BAN_AFTER_ORDER && $userData->getGroupId() == CUSTOMERS) {}
// .....
    }
}

// application/libraries/Models/User/User.php:88
class User
{
    public function isBanCustomerAfterOrder(): bool
    {
        return $this->ban == BAN_AFTER_ORDER && $this->isCustomer();
    }
    

// Существующий код
    public function isAgency()
    {
        return $this->sub_group == self::SG_AGENCY;
    }
    
    public function emptyPhone()
    {
        return empty($this->meta->phone);
    }
}

class Admin_Service_Log
{
    public static function getEventByBan($typeBan, $isLimitingBan)
    {
        if ($typeBan == BAN) {
            $adminAction = Admin_Service_Log::EVENT_ADMIN_BAN_USER;
        } elseif ($isLimitingBan) {
            $adminAction = Admin_Service_Log::EVENT_ADMIN_SET_ACCOUNT_LIMIT;
        } else {
            $adminAction = Admin_Service_Log::EVENT_ADMIN_BAN_USER;
        }
    }
}