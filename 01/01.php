<?php

// Первый принцип рефакторинга:
// 1. Лучший коментарий, тот без которого можно обойтись
// 2. Не соответствие коментария, коду

/**
 * @var Order $order
 */
if ($order->status == Order::STATUS_NEW || ($order->status == Order::STATUS_PAID && $order->deliveryDate > time() + 3600 * 24)) {
    echo 'button';
}

class Order
{
    const STATUS_NEW  = 1;
    const STATUS_PAID = 1;
    
    public $deliveryDate;
    public $status;
}