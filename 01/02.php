<?php

/**
 * @var Order $order
 */
if ($order->isNew() || ($order->isPaid() && !$order->isOnDelivery())) {
    echo 'button';
}

class Order
{
    const STATUS_NEW  = 1;
    const STATUS_PAID = 2;
    
    const DELIVERY_TIME = 3600 * 72;
    
    public $deliveryDate;
    public $status;
    
    public function isNew()
    {
        return $this->status == self::STATUS_NEW;
    }
    
    public function isPaid()
    {
        return $this->status == self::STATUS_PAID;
    }
    
    public function isOnDelivery()
    {
        return $this->deliveryDate < time() + self::DELIVERY_TIME;
    }
}