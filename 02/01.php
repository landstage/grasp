<?php

// Обладает данными для инициализации

// Если нет информации, как создать объект. То создание происходит снаружи объекта
$cart = new Cart(new SessionStorage('cart'), new Cost());
$cart->add(5, 7, 1000);

class Cart
{
    // Содержит
    // Обладает данными для инициализации
    public function add($id, $count, $price)
    {
        $current = isset($this->items[$id]) ? $this->items[$id]->getCount() : 0;
        $this->items[$id] = new CartItem($id, $current + $count, $price);
        $this->saveItems();
    }
}