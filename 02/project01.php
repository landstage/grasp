<?php

// application/libraries/App/AwardBest/Model/Award.php:5

/**
 * Запись статистические данных в таблицу
 */
class AwardBest_Model_Award implements Awards_Model_Award_DisplayInterface
{
    // Проставляет награды за предыдущий день авторам
    public function awardsDelivery(): array
    {
// .....
        $positionService = new \App\Rating\Calculator\Positions($this->projectId);
        foreach ($subjectsSet as $key => $subject) {
            
            //получаем победителей для текущего предмета
            $winners = $positionService->getTopMembersInList($subjectList, 10, 0);
        }
// .....
    }
    
    public function getAwardUserProgressText(int $userId): string
    {
        $dao = new AwardBest_Dao_Awards();
        $day = intval(date('d'));
        $month = intval(date('m'));
        $year = intval(date('Y'));
        if (!$dao->isAuthorGetAward(
            $userId,
            $day,
            $month,
            $year
        )
        ) {
            return AwardBest_Service_Awards::BEST_USERPROCESS_NO;
        }
        
        return AwardBest_Service_Awards::BEST_USERPROCESS_TEMPLATE;
    }
}
?>

<?php
class AwardBest_Model_AwardNew implements Awards_Model_Award_DisplayInterface
{
    public function __construct(AwardBest_Dao_Awards $dao)
    {
    
    }
    
    public static function awardsDelivery()
    {
    
    }
}
