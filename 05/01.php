<?php

class User
{
    private $nick;
    private $email;
    
    public function __construct($email, $nick)
    {
        $this->nick = $nick;
        $this->email = $email;
    }
    
    /**
     * @param mixed $nick
     */
    public function changeNick($nick): void
    {
        $this->nick = $nick;
    }
    
    public function filterEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}