<?php

use Swift_Mailer;
use Swift_MailTransport;
use Swift_Message;

require_once __DIR__ . '/vendor/autoload.php';

$transport = new Swift_MailTransport();

$transport = Swift_SmtpTransport::newInstance('smtp.yandex.ru', 443, 'tls')
    ->setUsername('username@ya.ru')
    ->setPassword('password');

class EchoTransport implements Swift_Transport
{
    public function isStarted()
    {
        return true;
    }
    
    public function start()
    {
    }
    
    public function stop()
    {
    }
    
    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        echo 'Send: ' . $message->getSubject() . PHP_EOL;
        
        return 1;
    }
    
    public function registerPlugin(Swift_Events_EventListener $plugin)
    {
    }
}

$mailer = new Swift_Mailer($transport);

$message = Swift_Message::newInstance('Subject')
    ->setFrom(['mail@test.ru' => 'username'])
    ->setTo(['mail@test.ru' => 'username'])
    ->setBody('It is live');

$mailer->send($message);